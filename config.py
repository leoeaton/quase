invar_imp = 0 #The weight for the invariance
s2s_imp= 0.55 #The weight for the self-reconstruction loss; the weight for the outcome_loss is 1-s2s_imp
kl_imp = 0.5 #The weight for kl

diff_imp = 0.1 #The weight for diff_loss
sim_imp = 0.4 #The weight for sim_loss
discrim_imp = 0.4 #The weight for dual-reconstruction loss

md = 50 #The dimension of the outcome variable
mdc = 50 #The dimension of the content variable

max_epochs = 20 # The num of maximum epochs

emb_path = "./glove.6B.200d.txt" #the path of the pretrained embedding


target_rating = 5 #The target rating
revised_sentence = './out/revised_sentences' #the file to save the revised sentences, each line containing a revised sentence
edit_distance = "./out/edit_distance" #the file to save the edit_distance
