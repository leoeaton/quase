
from __future__ import print_function
from __future__ import division
import numpy as np
import json
import random
import quase
from scipy.stats.stats import pearsonr
import pickle
import time
from file_reader import file_reader
from options import load_arguments
from config import *


if __name__ == '__main__':
    args = load_arguments()
    fr = file_reader()
    single_sents, raw_data_outcome, pair_sents0, pair_sents1, pair_inc, pair_dec, raw_outcomes_sent0, raw_outcomes_sent1, vocab = fr.read_json(args.train)
    data_outcome = np.array(raw_data_outcome)

    outcomes_sent0 = np.array(raw_outcomes_sent0)
    outcomes_sent1 = np.array(raw_outcomes_sent1)
    
    vocab = sorted(vocab)
    vocab_size = len(vocab)
    with open("vocab","w") as f:
        for word in vocab:
            f.write(word+"\n")
        
    glove_embedding = fr.read_embedding(emb_path, vocab, embedding_size = 200)
    model = quase.quase(model_type ="joint",max_seq_length = 15, vocab = vocab, glove_embedding = glove_embedding, target_rating = target_rating, use_unknown = False, learning_rate = 1e-3, embedding_dim = 200,  memory_dim = md, memory_dim_content = mdc,use_sigmoid = False, outcome_var = np.var(np.array(data_outcome)))

    if args.train:
        if not args.save_model:
            print ("please use option --save_model to specify the path+name for saving the model\n")
        else:
            train_losses, val_outcome_errors, val_seq2seq_errors = model.train(
                    single_sents, 
                    pair_sents0, pair_sents1,pair_inc, pair_dec, data_outcome,outcomes_sent0, outcomes_sent1,
                     seq2seq_importanceval = s2s_imp,
            pair_importance_diff = diff_imp, pair_importance_sim = sim_imp, discrim_importance = discrim_imp,
                    kl_importanceval = kl_imp, invar_importanceval = invar_imp,
                    max_epochs = max_epochs, batch_size = 64,invar_batchsize = 16, save_name = args.save_model)
    if args.test:
        if not args.load_model:
            print ("please use option --load_model to specify the path+name for saving the model\n")
        else:
            model.restore(args.load_model)
            with open(args.test) as f:
                testsents = json.load(f)
            sent_writer = open(revised_sentence,"w")
            edit_writer = open(edit_distance, "w")
            testsents = testsents[:10]
            
            log_alpha = -100000 # controls amount of revision allowed (smaller alpha = more allowed revision)
            for idx, init_seq in enumerate(testsents):
                if idx%1000 == 0:
                    print (idx)
                revision_results = model.barrierGradientRevise([init_seq], log_alpha= log_alpha, outcomeopt_learn_rate = 1.0, max_outcomeopt_iter = 10000, use_adaptive = False)
                x_star, z_star, inferred_improvement, outcome_star, reconstruct_init, z_init, outcome_init, avg_sigma_init, edit_dist = revision_results
                sent_writer.write((' '.join(x_star))+'\n')
                edit_writer.write(str(edit_dist) + '\n')
