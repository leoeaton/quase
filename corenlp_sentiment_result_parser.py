import sys

def parse(infile, outfile):
    output = open(outfile,'w')
    with open(infile,'r') as lines:
        block = []
        for line in lines:
            block.append(line)
            if line.startswith(' '):
                for s_line in lines:
                    if s_line.startswith(' '):
                        continue
                    else:
                        #print block
                        #print '\n'
                        get_score_vec(block,output)
                        block=[s_line]
                        break

        get_score_vec(block,output)

    output.close()

def get_score_vec(block,output):
    if len(block) != 3:
        print 'ERROR: the current parsing block length is not 3\n'
        print block
        if len(block) > 3:
            block = block[-3:]
        else:
            return []
    toks = block[2].strip().split()
    if len(toks) != 6:
        print 'ERROR: the current parsing scoreslength is not 6\n'
        print toks
        exit()
    probs = [float(i) for i in toks[1:6]]
    rating = cal_rating(probs)
    output.write(str(rating) + '\t' + block[0])


def cal_rating(vec):
    ret = 0.0
    for i in range(1,6):
        ret = ret + float(i) * vec[i-1]
    return ret



if __name__ == "__main__":
    #parse('/data1/express/leoeliao/s2bs/data/yelp/nips_data/made_data/subfiles/merge_nips_neu_60w.shuf.sent_corenlp',
     #       '/data1/express/leoeliao/s2bs/data/yelp/nips_data/made_data/subfiles/merge_nips_neu_60w.shuf.sent_corenlp_rating')

    infile=sys.argv[1]
    outfile=sys.argv[2]
    parse(infile, outfile)
