#!/bin/bash

# sh sentiment_parser.sh test.txt

infile=$1
crnt_dir=`pwd`
file_dir=${crnt_dir}/out
out_dir=${crnt_dir}/rating
corenlp_dir=${crnt_dir}/corenlp

source ~/.profile
cd $corenlp_dir
java -cp "*:." -mx5g mySentimentPipeline -file ${file_dir}/$infile -output probabilities> $out_dir/${infile}_senti

cd $crnt_dir
python2 corenlp_sentiment_result_parser.py $out_dir/${infile}_senti $out_dir/${infile}_senti.rating
