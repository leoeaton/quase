#from nltk import work_tokenize as wt
import csv
import numpy as np
import random
import json
class file_reader():
    def read_json(self, file_path):
        vocab = set()
        
        with open(file_path) as f:
            data_seq = json.load(f)
        single_sents = [ele[0][0] for ele in data_seq]
        for sent in single_sents:
            for word in sent:
                vocab.add(word)
        #data_seq = data_seq[:10]
        single_sents = [ele[0][0] for ele in data_seq]
        outcomes = [ele[0][1] for ele in data_seq]
        pair_sents0 = [ele[1][0] for ele in data_seq]
        pair_sents1 = [ele[1][1] for ele in data_seq]
        pair_inc = [random.choice(ele[2][0]) for ele in data_seq]
        pair_dec = [random.choice(ele[2][1]) for ele in data_seq]
        outcomes_sent0 = [ele[3][0] for ele in data_seq]
        outcomes_sent1 = [ele[3][1] for ele in data_seq]
        print len(single_sents) 
        return single_sents, outcomes, pair_sents0, pair_sents1, pair_inc, pair_dec ,outcomes_sent0, outcomes_sent1, vocab 

    def read_embedding(self, file_path, vocab, embedding_size):
        flag = 0
        dict_vocab = set(vocab)
        embedding = np.random.random_sample((len(vocab), embedding_size)) - 0.5
        with open(file_path) as f:
            for line in f:
                parts = line.split()
                word = parts[0]
                vec = np.array([float(x) for x in parts[1:]])
                if word in dict_vocab:
                    flag += 1
                    embedding[vocab.index(word)] = vec
                    #if flag%1000 == 0:
                    #    print flag
        return np.float32(embedding)
