#Guildlines for QuaSE
The source code is the for the paper 
**"Yi Liao, Lidong Bing, Piji Li, Shuming Shi, Wai Lam, and Tong Zhang. QuaSE: Sequence Editing under Quantifiable Guidance. EMNLP 2018"**

Link to the paper: [https://arxiv.org/abs/1804.07007](https://arxiv.org/abs/1804.07007)

It was developed with tensorflow1.2.0 and python2.7. 
It was developed based on the [source code](https://bitbucket.org/jwmueller/sequence-to-better-sequence/src/master/) for the paper 
"J. Mueller, D. Gifford, and T. Jaakkola. Sequence to Better Sequence: Continuous Revision of Combinatorial Structures. ICML 2017"

## Dataset
The dataset for QUASE can be downloaded [here](http://www1.se.cuhk.edu.hk/~textmine/dataset/quase/quase-data.tar.gz).
It contains two files: ```train.json``` and ```test.json```.
The ```train.json``` is a single json file containing a list of training samples.
Format of each sample is : \[\[single_sent, single_sent_outcome\],
\[sent_pair1, sent_pair2,jaccard_idx\],
\[sent2_pair2-sent1_pair1, sent_pair1_sent_pair2\](stands for word difference),
\[sent_pair1_outcome,sent_pair2_outcome\]

## Steps to run the model
### Parameter Configuration: 
The program takes the parameter values in config.py. You can adjust the values of parameters by modifying config.py 
### Step 1. train the model: 
```CUDA_VISIBLE_DEVICES=gpuid python Example.py --train TRAINING_DATA --save_model SAVE_MODEL_NAME```

Note: The format of the TRAINING_DATA should strictly follow train.json in the provided dataset.
SAVE_MODEL_NAME is the name of the saved model, which will be used for revising the sentences.

### Step 2. revise the sentenses:
```CUDA_VISIBLE_DEVICES=gpuid python Example.py --test  TESTING_DATA --load_model LOAD_MODEL_NAME```

Note: The format of the TESTING_DATA should strictly follow test.json in the provided dataset.
LOAD_MODEL_NAME is the name of the saved model in the step 1.

## Steps to compute the rating for sentences automatically with StanfordCoreNLP
### Step 1. prepare StanfordCoreNLP
Download [StanfordCoreNLP](https://stanfordnlp.github.io/CoreNLP/) java package and uncompress all the files to the folder ```./stanfordcorenlp```.
### Step 2. compute the rating 
```sh sentiment_parser.sh FILE```

Note: ```FILE``` contains a list of sentences stored line by line. 
The first step in ```sentiment_parser.sh``` calls sentimentpipeline in StanfordCoreNLP. 
However, I did not find a solution to run the sentimentpipeline in StanfordCoreNLP that processes a file line by line, so I wrote the code ```./stanfordcorenlp/mySentimentPipeline.java```.
Compile ```mySentimentPipeline.java``` first.

A sample command:

```sh sentiment_parser.sh samplefile```. 
