import sys
import argparse
import pprint
def load_arguments():
    argparser = argparse.ArgumentParser(sys.argv[0])
    argparser.add_argument('--train', type = str, default = '')
    argparser.add_argument('--test', type = str, default = '')
    argparser.add_argument('--load_model', type= str, default = '')
    argparser.add_argument('--save_model', type = str, default = '')

    args = argparser.parse_args()
    pp = pprint.PrettyPrinter(indent = 4)
    pp.pprint(vars(args))

    return args
