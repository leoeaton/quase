
from __future__ import print_function
from __future__ import division
import math
import numpy as np
import scipy.linalg
import tensorflow as tf
from tensorflow.contrib.legacy_seq2seq.python.ops.seq2seq import *
import tempfile
import warnings
from methods import *

class quase(object):
    """
    variable name without index is related to single sentence modeling. For example, self.labels
    variable name without index 1 or 2 as suffix is related to pseudo-parallel sentence modeling. For example, self.labels1 and self.labels2.
    """
    
    def __init__(self, model_type, max_seq_length, vocab, glove_embedding, target_rating, use_unknown = False,
                 learning_rate = 1e-3,
                 embedding_dim = None, memory_dim = 100, memory_dim_content = 200, 
                 which_outcomeprediction = 'nonlinear', 
                 use_sigmoid = False, 
                outcome_var = None, 
                 logdir=None):
        self.max_seq_length = max_seq_length
        self.vocab = vocab
        self.vocab.append('<PAD>')
        self.PAD_ID = self.vocab.index('<PAD>')
        self.use_unknown = use_unknown
        if use_unknown:
            self.vocab.append('<UNK>')
            self.UNK_ID = self.vocab.index('<UNK>')
        self.vocab_size = len(vocab)
        # Architecture parameters:
        if embedding_dim is None:
            self.embedding_dim = self.vocab_size - 1
        else:
            self.embedding_dim = embedding_dim
        self.data_type = tf.float32 # tf.float16 # tf.float32 # finer-precision alternative.
        self.memory_dim = memory_dim
        if logdir is None:
            self.logdir = tempfile.mkdtemp()
        else: 
            self.logdir = logdir
        print("logdir:",self.logdir)
        # To summarize training, run: tensorboard --logdir=/var/folders/0f/h0w0z9jj6ns3097h5zplwl8c0000gn/T/tmpz9stf063
        # Training parameters:
        self.learning_rate = learning_rate
        self.outcome_var = 1.0 # outcome loss is divided by this value to re-scale. Should be variance of outcomes in training data. 
        if outcome_var is not None:
            print("Rescaling prediction MSE loss by outcome-variance =", outcome_var + 0.1)
            self.outcome_var = outcome_var + 0.1
        print ("starting initializing variables")
        self.enc_inp = [tf.placeholder(tf.int32, shape=(None,),
                                name="inp%i" % t)
                 for t in range(self.max_seq_length)]
        self.labels = [tf.placeholder(tf.int32, shape=(None,),
                              name="labels%i" % t)
                for t in range(self.max_seq_length)]
        self.weights = [tf.ones_like(labels_t, dtype=tf.float32)
                 for labels_t in self.labels] # weight of each sequence position in cross-entropy loss.
        self.outcomes = tf.placeholder(tf.float32, shape=(None,1),
                              name="outcomes") # actual outcome-labels for each sequence.
        self.outcomes_sent1 = tf.placeholder(tf.float32, shape=(None,1),
                              name="outcomes") # actual outcome-labels for each sequence.
        self.outcomes_sent2 = tf.placeholder(tf.float32, shape=(None,1),
                              name="outcomes") # actual outcome-labels for each sequence.
        # Decoder input: prepend some "GO" token and drop the final
        # token of the encoder input
        self.dec_inp = ([tf.zeros_like(self.enc_inp[0], dtype=np.int32, name="GO")]
            + self.enc_inp[:-1])
        #Inputs for the sentence pairs:
        self.pair_inp1 = [tf.placeholder(tf.int32, shape = (None, ), name = "pair_inp1%i" %t) for t in range(self.max_seq_length)]
        self.pair_inp2 = [tf.placeholder(tf.int32, shape = (None, ), name = "pair_inp2%i" %t) for t in range(self.max_seq_length)]
        self.labels1 = [tf.placeholder(tf.int32, shape=(None, ), name="labels1%i" %t) for t in range(self.max_seq_length)]
        self.labels2 = [tf.placeholder(tf.int32, shape=(None, ), name="labels2%i" %t) for t in range(self.max_seq_length)]
        self.pair_inc = tf.placeholder(tf.int32, shape = (None,), name = "pair_inc") #Q: whether the shape is correct
        self.pair_dec = tf.placeholder(tf.int32, shape = (None,), name = "pair_dec") #Q: whether the shape is correct
        self.dec_inp_sent1 = ([tf.zeros_like(self.pair_inp1[0], dtype=np.int32, name="GO_sent1")] + self.pair_inp1[:-1])
        self.dec_inp_sent2 = ([tf.zeros_like(self.pair_inp2[0], dtype=np.int32, name="GO_sent2")] + self.pair_inp2[:-1])

        # Setup RNN components:
        rnn_type = 'GRU' # can be: 'GRU' or 'DeepGRU' or 'LSTM'
        num_rnnlayers = 2 # only for DeepGRU.
        rnn_cell = tf.nn.rnn_cell
        if rnn_type != 'GRU':
            raise ValueError("We donnot support other cells except GRU currently. The cell_type is set to GRU automatically!")
        self.cell = rnn_cell.GRUCell(memory_dim)
        self.cell_content = rnn_cell.GRUCell(memory_dim_content)
        self.cell_rec = rnn_cell.GRUCell(memory_dim_content + memory_dim)
        print("RNN type: ", rnn_type, "   cell_state_size:",self.cell.state_size, "    cell_content_state_size:", self.cell_content.state_size)

        # The following parts defines the variables/weights for the outcome prediction part
        self.which_outcomeprediction = which_outcomeprediction
        if self.which_outcomeprediction == 'linear':  # Linear Outcome prediction
            self.weights_pred = tf.Variable(tf.truncated_normal([self.cell.state_size, 1],dtype=self.data_type), 
                                            name="coefficients", trainable=True)
            self.biases_pred = tf.Variable(tf.zeros([1],dtype=self.data_type), 
                                           name="bias", trainable=True)
            self.use_sigmoid = False # no sigmoid used in linear outcome predictions.
        else: # Nonlinear Outcome prediction:
            self.weights_pred = dict()
            self.biases_pred = dict()
            self.weights_pred['W1'] = tf.Variable(tf.truncated_normal([self.cell.state_size, self.cell.state_size],dtype=self.data_type), name="W1", trainable=True)
            self.weights_pred['W2'] = tf.Variable(tf.truncated_normal([self.cell.state_size,1],dtype=self.data_type), name="W2", trainable=True)
            self.biases_pred['B1'] = tf.Variable(tf.zeros([1, self.cell.state_size],dtype=self.data_type), name="B1", trainable=True)
            self.biases_pred['B2'] = tf.Variable(tf.zeros([1],dtype=self.data_type), name="B2", trainable=True)
            self.use_sigmoid = use_sigmoid # if True, then outcome-predictions are constrained to [0,1]
        self.params_pred = (self.weights_pred, self.biases_pred) # tuple of outcome-prediction parameters.
        print("Type of outcome prediction: ", self.which_outcomeprediction, "   All predictions in [0,1]: ", self.use_sigmoid)
        
        # Parameters to produce variational posterior after the hidden state is obtained:
        self.epsilon_vae = tf.placeholder(tf.float32, shape=(None,self.cell.state_size),
                              name="epsilon_vae") # noise for VAE
        weights_mu = tf.Variable(tf.truncated_normal([self.cell.state_size, self.cell.state_size],dtype=self.data_type), 
                                        name="weights_mu", trainable=True)
        biases_mu = tf.Variable(tf.zeros([1,self.cell.state_size],dtype=self.data_type), 
                                       name="biases_mu", trainable=True)
        inital_sigma_bias = 12.0 # want very large so variance in posteriors is ~0 at beginning of training.
        weights_sigma1 = tf.Variable(tf.truncated_normal([self.cell.state_size, self.cell.state_size],dtype=self.data_type), 
                                        name="weights_sigma1", trainable=True)
        weights_sigma2 = tf.Variable(tf.truncated_normal([self.cell.state_size, self.cell.state_size],dtype=self.data_type), 
                                        name="weights_sigma2", trainable=True)
        biases_sigma1 = tf.Variable(tf.zeros([1,self.cell.state_size],dtype=self.data_type),
                                    name="biases_sigma1", trainable=True)
        biases_sigma2 = tf.Variable(tf.fill([1,self.cell.state_size],value=inital_sigma_bias),
                                    dtype=self.data_type,
                                    name="biases_sigma2", trainable=True)
        mu_params = dict()
        mu_params['weights'] = weights_mu
        mu_params['biases'] = biases_mu
        sigma_params = dict()
        sigma_params['weights1'] = weights_sigma1
        sigma_params['biases1'] = biases_sigma1
        sigma_params['weights2'] = weights_sigma2
        sigma_params['biases2'] = biases_sigma2
        self.variational_params = (mu_params, sigma_params)

        # Parameters to produce variational posterior for the content variable:
        self.epsilon_vae_content = tf.placeholder(tf.float32, shape=(None,self.cell_content.state_size),
                              name="epsilon_vae_content") # noise for VAE regarding the content variable
        weights_content_mu = tf.Variable(tf.truncated_normal([self.cell_content.state_size, self.cell_content.state_size],dtype=self.data_type), 
                                        name="weights_content_mu", trainable=True)
        biases_content_mu = tf.Variable(tf.zeros([1,self.cell_content.state_size],dtype=self.data_type), 
                                       name="biases_content_mu", trainable=True)

        weights_content_sigma1 = tf.Variable(tf.truncated_normal([self.cell_content.state_size, self.cell_content.state_size],dtype=self.data_type), 
                                        name="weights_sigma1", trainable=True)
        weights_content_sigma2 = tf.Variable(tf.truncated_normal([self.cell_content.state_size, self.cell_content.state_size],dtype=self.data_type), 
                                        name="weights_content_sigma2", trainable=True)
        biases_content_sigma1 = tf.Variable(tf.zeros([1,self.cell_content.state_size],dtype=self.data_type),
                                    name="biases_content_sigma1", trainable=True)
        biases_content_sigma2 = tf.Variable(tf.fill([1,self.cell_content.state_size],value=inital_sigma_bias),
                                    dtype=self.data_type,
                                    name="biases_content_sigma2", trainable=True)
        mu_params_content = dict()
        mu_params_content['weights'] = weights_content_mu
        mu_params_content['biases'] = biases_content_mu
        sigma_params_content = dict()
        sigma_params_content['weights1'] = weights_content_sigma1
        sigma_params_content['biases1'] = biases_content_sigma1
        sigma_params_content['weights2'] = weights_content_sigma2
        sigma_params_content['biases2'] = biases_content_sigma2
        self.variational_params_content = (mu_params_content, sigma_params_content)

        #Network Parameters and Variables:
        n_hidden_1 = 256
        n_hidden_2 = 256
        n_input = 2*self.embedding_dim
        n_classes = self.cell.state_size

       
        network_weights = {"h1": tf.Variable(tf.truncated_normal([n_input, n_hidden_1]), dtype = self.data_type), "h2": tf.Variable(tf.truncated_normal([n_hidden_1, n_hidden_2]), dtype = self.data_type), "out": tf.Variable(tf.truncated_normal([n_hidden_2, n_classes]), dtype = self.data_type)}

        network_biases = {"b1": tf.Variable(tf.truncated_normal([n_hidden_1]), dtype = self.data_type), "out":tf.Variable(tf.truncated_normal([n_classes]), dtype = self.data_type), "b2":tf.Variable(tf.truncated_normal([n_hidden_2]), dtype = self.data_type)}

        #Inputs and Output of the difference between two sentences:
        self.latent_difference = tf.placeholder(tf.float32, shape = (None, n_classes))

        #randomly initialize the embeddings
        #self.embedding = tf.Variable(tf.random_uniform([self.vocab_size, self.embedding_dim], -1.0, 1.0))
        self.embedding = tf.Variable(glove_embedding)

        emb_enc_inputs = [tf.nn.embedding_lookup(self.embedding, self.enc_inp[t]) for t in range(self.max_seq_length)]
        emb_dec_inputs = [tf.nn.embedding_lookup(self.embedding, self.dec_inp[t]) for t in range(self.max_seq_length)]
        emb_enc_inputs_sent1 = [tf.nn.embedding_lookup(self.embedding, self.pair_inp1[t]) for t in range(self.max_seq_length)]
        emb_enc_inputs_sent2 = [tf.nn.embedding_lookup(self.embedding, self.pair_inp2[t]) for t in range(self.max_seq_length)]
        emb_dec_inputs_sent1 = [tf.nn.embedding_lookup(self.embedding, self.dec_inp_sent1[t]) for t in range(self.max_seq_length)]
        emb_dec_inputs_sent2 = [tf.nn.embedding_lookup(self.embedding, self.dec_inp_sent2[t]) for t in range(self.max_seq_length)]
        emb_inc = tf.nn.embedding_lookup(self.embedding, self.pair_inc)
        emb_dec = tf.nn.embedding_lookup(self.embedding, self.pair_dec)
        self.word_difference = tf.concat([emb_inc,emb_dec], 1)

        # Get encoding and outcome prediction:
        createDeterministicVar(emb_enc_inputs, self.cell, self.embedding, self.vocab_size, self.embedding_dim, scope = "outcome_var")
        self.z0, self.sigma0 = variationalEncoding(emb_enc_inputs, self.cell,
                self.vocab_size, self.embedding_dim, self.variational_params, scope = "outcome_var")
        self.outcome0 = outcomePrediction(self.z0, self.params_pred, self.which_outcomeprediction, self.use_sigmoid)
        self.z0_sent1, self.sigma0_sent1 = variationalEncoding(emb_enc_inputs_sent1, self.cell,
                self.vocab_size, self.embedding_dim, self.variational_params, scope = "outcome_var")
        self.outcome0_sent1 = outcomePrediction(self.z0_sent1, self.params_pred, self.which_outcomeprediction, self.use_sigmoid)
        self.z0_sent2, self.sigma0_sent2 = variationalEncoding(emb_enc_inputs_sent2, self.cell,
                self.vocab_size, self.embedding_dim, self.variational_params, scope = "outcome_var")
        self.outcome0_sent2 = outcomePrediction(self.z0_sent2, self.params_pred, self.which_outcomeprediction, self.use_sigmoid)

        # Get encoding for the content:
        createDeterministicVar(emb_enc_inputs, self.cell_content, self.embedding, self.vocab_size, self.embedding_dim, scope = "content")
        self.z0_content, self.sigma0_content = variationalEncoding(emb_enc_inputs, self.cell_content, self.vocab_size, self.embedding_dim, self.variational_params_content, scope = "content")

        #createDeterministicVar(self.pair_inp1, self.cell_content, self.vocab_size, self.embedding_dim)
        self.z0_content_sent1, self.sigma0_content_sent1 = variationalEncoding(emb_enc_inputs_sent1, self.cell_content, self.vocab_size, self.embedding_dim, self.variational_params_content, scope = "content")

        #createDeterministicVar(self.pair_inp2, self.cell_content, self.vocab_size, self.embedding_dim)
        self.z0_content_sent2, self.sigma0_content_sent2 = variationalEncoding(emb_enc_inputs_sent2, self.cell_content, self.vocab_size, self.embedding_dim, self.variational_params_content, scope = "content")

        # Used at train-time  for the reconstruction of the original sentences using both the outcome variable and content variable:
        createDeterministicVar(emb_dec_inputs, self.cell_rec, self.embedding, self.vocab_size, self.embedding_dim, scope = "rec")
        self.traindecoderprobs = getDecoding(tf.concat([tf.add(self.z0,tf.multiply(self.sigma0,self.epsilon_vae)),tf.add(self.z0_content,tf.multiply(self.sigma0_content,self.epsilon_vae_content))],axis=1), emb_dec_inputs, self.cell_rec, self.embedding, self.vocab_size, self.embedding_dim, feed_previous = False, scope = "rec")

        # Used at train-time for the generating fake sentences:
        self.fake_sent1 = getDecoding(tf.concat([tf.add(self.z0_sent1, tf.multiply(self.sigma0_sent1, self.epsilon_vae)), tf.add(self.z0_content_sent2, tf.multiply(self.sigma0_content_sent2, self.epsilon_vae_content))], axis = 1), emb_dec_inputs_sent1, self.cell_rec, self.embedding, self.vocab_size, self.embedding_dim, feed_previous = False, scope = "rec")
        self.fake_sent2 = getDecoding(tf.concat([tf.add(self.z0_sent2, tf.multiply(self.sigma0_sent2, self.epsilon_vae)), tf.add(self.z0_content_sent1, tf.multiply(self.sigma0_content_sent1, self.epsilon_vae_content))], axis = 1), emb_dec_inputs_sent2, self.cell_rec, self.embedding, self.vocab_size, self.embedding_dim, feed_previous = False, scope = "rec")
        '''
        # Used at test-time:
        self.decodingprobs0 = getDecoding(self.z0, self.dec_inp, self.cell, 
                                          self.vocab_size, self.embedding_dim, 
                                          feed_previous = True)
        '''
        # invariance portion
        self.invar_target = tf.placeholder_with_default(tf.zeros([1,1],dtype=self.data_type), 
                          shape=(None,1), name="invar_target")
        self.invar_inp = [tf.placeholder_with_default(tf.zeros(1,dtype=tf.int32), 
                          shape=(None,), name="invar_inp%i" % t)
                          for t in range(self.max_seq_length)] # inputs for invariance prediction.
        emb_invar_inputs = [tf.nn.embedding_lookup(self.embedding, self.invar_inp[t]) for t in range(self.max_seq_length)]
        self.invar_pred = outcomePrediction(variationalEncoding(emb_invar_inputs, self.cell,
                            self.vocab_size, self.embedding_dim, self.variational_params, scope = "outcome_var")[0], 
                           self.params_pred, self.which_outcomeprediction, self.use_sigmoid)
        self.loss_inv = tf.reduce_mean(tf.square(self.invar_target - self.invar_pred))
        tf.summary.scalar("loss_inv", self.loss_inv) # invariance loss.
        self.invar_importance = tf.placeholder("float") # bigger = more invariance.
        # Loss Functions:
        loss_discrim1 = sequence_loss(self.fake_sent1, self.labels1, self.weights, average_across_timesteps = False)
        loss_discrim2 = sequence_loss(self.fake_sent2, self.labels2, self.weights, average_across_timesteps = False)
        self.loss_discrim = tf.add(loss_discrim1, loss_discrim2) #dual-reconstruction loss
        self.seq2seq_importance = tf.placeholder("float") # between 0 and 1.
        self.kl_importance = tf.placeholder("float") # between 0 and 1.
        
        self.loss_lik = sequence_loss(self.traindecoderprobs, self.labels, self.weights, average_across_timesteps = False) #self-reconstruction loss
        self.loss_prior = tf.reduce_mean(-(0.5/self.cell.state_size) * tf.reduce_sum(1.0 + tf.log(tf.square(self.sigma0)) - tf.square(self.z0) - tf.square(self.sigma0), 1)) + tf.reduce_mean(-(0.5/self.cell_content.state_size) * tf.reduce_sum(1.0 + tf.log(tf.square(self.sigma0_content)) - tf.square(self.z0_content) - tf.square(self.sigma0_content), 1)) + tf.reduce_mean(-(0.5/self.cell.state_size) * tf.reduce_sum(1.0 + tf.log(tf.square(self.sigma0_sent1)) - tf.square(self.z0_sent1) - tf.square(self.sigma0_sent1), 1)) + tf.reduce_mean(-(0.5/self.cell_content.state_size) * tf.reduce_sum(1.0 + tf.log(tf.square(self.sigma0_content_sent1)) - tf.square(self.z0_content_sent1) - tf.square(self.sigma0_content_sent1), 1)) + tf.reduce_mean(-(0.5/self.cell.state_size) * tf.reduce_sum(1.0 + tf.log(tf.square(self.sigma0_sent2)) - tf.square(self.z0_sent2) - tf.square(self.sigma0_sent2), 1)) + tf.reduce_mean(-(0.5/self.cell_content.state_size) * tf.reduce_sum(1.0 + tf.log(tf.square(self.sigma0_content_sent2)) - tf.square(self.z0_content_sent2) - tf.square(self.sigma0_content_sent2), 1)) #kl-loss
        priormean_scalingfactor = 0.1
        self.loss_priormean = tf.reduce_mean((0.5*priormean_scalingfactor/self.cell_content.state_size)*tf.reduce_sum(tf.square(self.z0_content),1))
        # Loss Fuctions for the sentence pair:
        self.pair_importance_diff = tf.placeholder("float")
        self.pair_importance_sim = tf.placeholder("float")
        self.discrim_importance = tf.placeholder("float")
        self.output_perceptron = multilayer_perceptron(self.word_difference, network_weights, network_biases)
        self.code_difference = self.z0_sent1 - self.z0_sent2
        self.loss_diff = tf.reduce_mean(tf.square(self.output_perceptron - self.code_difference))
        self.loss_sim = tf.reduce_mean(tf.square(self.z0_content_sent1 - self.z0_content_sent2))
        #self.loss_sim = tf.reduce_mean(tf.square(self.z0_content - self.z0_content_sent1))
        # Loss Functions for the discriminator
        # self.loss_pred = tf.reduce_mean(tf.abs(self.outcome0 - self.outcomes)) # l1 loss.
        self.loss_pred = tf.reduce_mean(tf.square(self.outcome0 - self.outcomes)) + tf.reduce_mean(tf.square(self.outcome0_sent1 - self.outcomes_sent1)) + tf.reduce_mean(tf.square(self.outcome0_sent2 - self.outcomes_sent2))
        if model_type == "joint":
            self.loss_joint = self.seq2seq_importance*(self.loss_lik + (self.loss_prior*self.kl_importance)) + ((1.0-self.seq2seq_importance)/self.outcome_var)*self.loss_pred + self.loss_diff * self.pair_importance_diff + self.loss_sim * self.pair_importance_sim + self.discrim_importance * (self.loss_discrim)
            #self.loss_joint = self.seq2seq_importance*(self.loss_lik + (self.loss_prior*self.kl_importance)) + ((1.0-self.seq2seq_importance)/self.outcome_var)*self.loss_pred + self.loss_diff * self.pair_importance_diff + self.loss_sim * self.pair_importance_sim + self.discrim_importance * (self.loss_discrim) + self.invar_importance*self.loss_inv
        tf.summary.scalar("loss_lik", self.loss_lik) # reconstruction loss.
        tf.summary.scalar("loss_prior", self.loss_prior) # variational KL loss.
        tf.summary.scalar("loss_pred", self.loss_pred) # outcome-prediction loss.
        tf.summary.scalar("loss_joint", self.loss_joint)
        tf.summary.scalar("loss_diff", self.loss_diff)
        tf.summary.scalar("loss_sim", self.loss_sim)
        tf.summary.scalar("loss_discrim", self.loss_discrim)

        # Training Optimization: 
        self.optimizer = tf.train.AdamOptimizer(learning_rate = self.learning_rate)
        self.gvs_joint = self.optimizer.compute_gradients(self.loss_joint)
        self.capped_joint = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in self.gvs_joint if grad is not None]
        self.train_joint = self.optimizer.apply_gradients(self.capped_joint) # joint training.
        self.train_autoenc = self.optimizer.minimize(self.loss_lik) # only train autoencoder.

        ## seq2BetterSeq portion of graph:
        self.z = tf.Variable(tf.zeros([1, self.cell.state_size]), name='latent_representation')
        self.inferred_outcome = outcomePrediction(self.z, self.params_pred, self.which_outcomeprediction, self.use_sigmoid)
        # Barrier function components:
        BARRIER_CONST = 1e6 # higher value = sharper barrier, so solutions are more accurately near constraint.
        self.zfirst = tf.placeholder_with_default(tf.zeros([1,self.cell.state_size],dtype=self.data_type), 
                            shape=(1,self.cell.state_size), name="zfirst")
        self.Amat = tf.placeholder_with_default(tf.diag(tf.ones(self.cell.state_size,dtype=self.data_type)), 
                          shape=(self.cell.state_size,self.cell.state_size), name="Amat")
        if target_rating == 6:
            self.revision_barrier_obj = self.inferred_outcome + tf.log(1 - tf.matmul(self.z-self.zfirst,tf.matmul(self.Amat,tf.transpose(self.z-self.zfirst))))/BARRIER_CONST
        elif target_rating == 0:
            self.revision_barrier_obj = -self.inferred_outcome + tf.log(1 - tf.matmul(self.z-self.zfirst,tf.matmul(self.Amat,tf.transpose(self.z-self.zfirst))))/BARRIER_CONST
        else:
            self.revision_barrier_obj = -tf.square(target_rating-self.inferred_outcome) + tf.log(1 - tf.matmul(self.z-self.zfirst,tf.matmul(self.Amat,tf.transpose(self.z-self.zfirst))))/BARRIER_CONST


        # OLD: self.z_grad = tf.gradients(self.inferred_outcome, self.z)[0]
        self.z_grad = tf.gradients(self.revision_barrier_obj, self.z)[0]
        self.x_hat = getDecoding(tf.concat([self.z,self.z0_content],axis=1), emb_dec_inputs, self.cell_rec, self.embedding, self.vocab_size, self.embedding_dim, scope = "rec")
        ## Run TF session:
        self.summary_op = tf.summary.merge_all()
        self.sess = tf.InteractiveSession()
        #self.sess = tf.InteractiveSession(config=config)
        # On orava: self.sess = tf.Session(config = tf.ConfigProto(device_count = {'GPU': 0}))
        self.sess.run(tf.global_variables_initializer()) # Intialize.
        self.summary_writer = tf.summary.FileWriter(self.logdir, self.sess.graph)
        self.saver = tf.train.Saver()
        # End of Seq2BetterSeq creation.
    
    """ Train model for entire epochs over dataset.
    Args:
        train_seqs: List where ith element is sequence from training data (list of strings).
        train_outcomes: List of outcomes for each sequence. Can be omitted if there are no labels for these sequences.
        test_seqs,test_outcomes: Validation set data (can be omitted). 
        which_model: Determines the model to train, one of: '', '', or 'joint' (to train both models).
        seq2seq_importanceval: The relative weighting of the seq2seq model's loss in the training, only used if which_model = 'joint'.
        kl_importanceval: The relative weight of the KL term in the VAE; if None, uses annealing to handle this.
        invar_importanceval: The relative weight of the Invariance term in the Loss. If 0, this term is ignored. If None, this term is annealed.
    """ 
    def train(self, train_seqs, train_pair_sent1, train_pair_sent2, train_pair_inc, train_pair_dec, train_outcomes,train_outcomes_sent1, train_outcomes_sent2, seq2seq_importanceval, pair_importance_diff, pair_importance_sim, discrim_importance, kl_importanceval, invar_importanceval, max_epochs, batch_size, invar_batchsize,save_name = None):
        test_seqs = None
        test_outcomes = None
        which_model = 'joint'
        n = len(train_seqs)
        anneal_kl = False
        if kl_importanceval is None:
            anneal_kl = True
            kl_importanceval = 0.0
        if len(train_outcomes) != n:
            raise ValueError("train_outcomes and train_seqs do not agree")
        idx = list(np.random.permutation(len(train_seqs)))
        #shuffle data
        seqs_shuffled = [train_seqs[i] for i in idx]
        seqs_pair_sent1_shuffled = [train_pair_sent1[i] for i in idx]
        seqs_pair_sent2_shuffled = [train_pair_sent2[i] for i in idx]
        pair_inc_shuffled = [train_pair_inc[i] for i in idx]
        pair_dec_shuffled = [train_pair_dec[i] for i in idx]
        if train_outcomes is not None:
            outcomes_shuffled = [train_outcomes[i] for i in idx]
            outcomes_sent1_shuffled = [train_outcomes_sent1[i] for i in idx]
            outcomes_sent2_shuffled = [train_outcomes_sent2[i] for i in idx]
        # Train in epochs:
        train_losses = []
        if which_model == 'joint':
            lik_losses = []
            pri_losses = []
            pred_losses = []
            sents_diff_losses = []
            sents_dim_losses = []
            sents_discrim_losses = []
        if test_seqs is not None:
            test_outcome_losses = []
            test_seq2seq_losses = []
        else:
            test_outcome_losses = None
            test_seq2seq_losses = None
        for epoch in range(max_epochs):
            print ("starting epoch:", epoch)
            #kl_importanceval = sigmoid(-5+10.0*epoch/max_epochs)
            #pair_importance_diff = sigmoid(-5+10.0*epoch/max_epochs)
            #pair_importance_sim = sigmoid(-5+10.0*epoch/max_epochs)
            #discrim_importance = sigmoid(-5+10.0*epoch/max_epochs)
            '''
            if epoch < max_epochs/5:
                kl_importanceval = 0.0
                pair_importance_diff =0.0
                pair_importance_sim = 0.0
                discrim_importance = 0.0
            else:
                kl_importanceval = sigmoid(-10.0 + 20.0*epoch/max_epochs) # anneal from sigmoid(-6) to sigmoid 6
                pair_importance_diff = sigmoid(-5+10.0*epoch/max_epochs)
                pair_importance_sim = sigmoid(-5+10.0*epoch/max_epochs)
                pair_importance_discrim = sigmoid(-5+10.0*epoch/max_epochs)
            '''
            print("kl_importance ", kl_importanceval, "diff_imp ", pair_importance_diff, "sim_imp ", pair_importance_sim, "discrim_imp ", discrim_importance)
            ind = 0
            avg_loss = 0 # avg training loss over epoch.
            if which_model == 'joint':
                avg_lik_loss = 0
                avg_pri_loss = 0
                avg_pred_loss = 0
                avg_sents_diff_loss = 0
                avg_sents_sim_loss = 0
                avg_discrim_loss = 0
                avg_inv_loss = 0
            num_batches = 0
            while ind < n: # Train batch:
                batch_outcomes = outcomes_shuffled[ind:min(n,ind+batch_size)]
                batch_outcomes_sent1 = outcomes_sent1_shuffled[ind:min(n,ind+batch_size)]
                batch_outcomes_sent2 = outcomes_sent2_shuffled[ind:min(n,ind+batch_size)]
                loss_batch, lik_loss_b, pri_loss_b, pred_loss_b, sents_diff_loss_b, sents_sim_loss_b, discrim_loss_b, inv_loss_b, summary_batch = self.train_batch(seqs_shuffled[ind:min(n,ind+batch_size)], 
                        seqs_pair_sent1_shuffled[ind: min(n, ind+batch_size)], seqs_pair_sent2_shuffled[ind: min(n, ind+batch_size)], pair_inc_shuffled[ind:min(n, ind+batch_size)], pair_dec_shuffled[ind:min(n, ind+batch_size)], batch_outcomes, batch_outcomes_sent1, batch_outcomes_sent2,which_model, seq2seq_importanceval, kl_importanceval, pair_importance_diff, pair_importance_sim, discrim_importance, invar_batchsize, invar_importanceval)
                self.summary_writer.add_summary(summary_batch, epoch+num_batches/np.ceil(n/(batch_size+1)))
                num_batches += 1.0
                avg_loss = ((num_batches-1)/num_batches)*avg_loss + loss_batch/num_batches
                avg_lik_loss = ((num_batches-1)/num_batches)*avg_lik_loss + lik_loss_b/num_batches
                avg_pri_loss = ((num_batches-1)/num_batches)*avg_pri_loss + pri_loss_b/num_batches
                avg_pred_loss = ((num_batches-1)/num_batches)*avg_pred_loss + pred_loss_b/num_batches
                avg_sents_diff_loss = ((num_batches-1)/num_batches)*avg_sents_diff_loss + sents_diff_loss_b/num_batches
                avg_sents_sim_loss = ((num_batches-1)/num_batches)*avg_sents_sim_loss + sents_sim_loss_b/num_batches
                avg_discrim_loss = ((num_batches-1)/num_batches)*avg_discrim_loss + discrim_loss_b/num_batches
                avg_inv_loss = ((num_batches-1)/num_batches)*avg_inv_loss + inv_loss_b/num_batches
                ind = ind + batch_size
            # Evaluate model on test-set:
            if test_seqs is not None:
                outcome_predictions, reconstructions, outcome_error, reconstruction_error, sigmas_pred  = self.predict(test_seqs, test_outcomes)
                test_outcome_losses.append(outcome_error)
                test_seq2seq_losses.append(reconstruction_error)
                train_losses.append(avg_loss)
                print('epoch:', epoch, '  train_loss=', avg_loss, '  val_outcome_error=', outcome_error, '  val_reconstruction_error=', reconstruction_error) 
                print('  avg predicted sigma=', sigmas_pred)
                print('  avg_lik_loss=',avg_lik_loss, '  avg_pri_loss=',avg_pri_loss, '  avg_pred_loss=',avg_pred_loss)
                lik_losses.append(avg_lik_loss)
                pri_losses.append(avg_pri_loss)
                pred_losses.append(avg_pred_loss)
            else:
                print('epoch:', epoch, '  train_loss=', avg_loss, ' lik_loss=', avg_lik_loss, 'pri_loss=', avg_pri_loss, 'pred_loss=', avg_pred_loss, 'sents_diff_loss=', avg_sents_diff_loss, 'sens_sim_loss=', avg_sents_sim_loss, 'discrim_loss=', avg_discrim_loss, 'inv_loss = ', avg_inv_loss)
            if (save_name is not None):
                # Save model every 10 epochs:
                self.saver.save(self.sess, save_name + '_' + str(epoch))
            self.summary_writer.flush()
        # End for over epochs.
        if save_name is not None:
            self.saver.save(self.sess, save_name + '_FINAL')
        return(train_losses, test_outcome_losses, test_seq2seq_losses)
    
    """ Train variational model using provided mini-batch.
    Args:
        batch_seqs: List of length batch_size where ith element is sequence (list of strings).
        batch_outcomes: List of length batch_size corresponding to outcomes for each sequence. Can be omitted if there are no labels for these sequences.
        which_model: Determines the model to train, one of: '', '', or 'joint' (to train both models).
        seq2seq_importanceval: The relative weighting of the seq2seq model's loss in the training, only used if which_model = 'joint'.
    """
    def train_batch(self, batch_seqs, batch_pair_sent1, batch_pair_sent2, batch_pair_inc, batch_pair_dec, batch_outcomes, batch_outcomes_sent1, batch_outcomes_sent2, which_model = 'joint', seq2seq_importanceval = 0.5, kl_importanceval = 0.0, pair_importance_diff = 0.5, pair_importance_sim = 0.5, discrim_importance = 0.5 , invar_batchsize = 16, invar_importanceval = 1):
        batch_size = len(batch_seqs)
        epsilons = np.random.normal(size=[batch_size,self.cell.state_size]) # noise variable
        epsilons_content = np.random.normal(size=[batch_size,self.cell_content.state_size]) # noise variable
        if len(batch_outcomes) != batch_size:
            raise ValueError("batch_seqs and batch_outcomes do not agree")
        batch_outcomes = np.array(batch_outcomes)
        batch_outcomes_sent1 = np.array(batch_outcomes_sent1)
        batch_outcomes_sent2 = np.array(batch_outcomes_sent2)
        batch_outcomes = batch_outcomes.reshape(len(batch_outcomes),1)
        batch_outcomes_sent1 = batch_outcomes_sent1.reshape(len(batch_outcomes_sent1),1)
        batch_outcomes_sent2 = batch_outcomes_sent2.reshape(len(batch_outcomes_sent2),1)
        index_seqs = self.convertToIndexSeq(batch_seqs) # reformatted seq in terms of indices in vocab
        index_pair_sent1 = self.convertToIndexSeq(batch_pair_sent1)
        index_pair_sent2 = self.convertToIndexSeq(batch_pair_sent2)
        index_pair_inc = [self.vocab.index(inc) for inc in batch_pair_inc]
        index_pair_dec = [self.vocab.index(dec) for dec in batch_pair_dec]
        # Dimshuffle to seq_len * batch_size
        index_array2 = np.array(index_seqs[:]).T # NON-converted inputs
        index_array2_pair_sent1 = np.array(index_pair_sent1[:]).T
        index_array2_pair_sent2 = np.array(index_pair_sent2[:]).T
        index_array2_pair_dec = np.array(index_pair_dec[:]).T
        index_array2_pair_inc = np.array(index_pair_inc[:]).T
        # index_array = np.array(index_seqs).T # NON-converted inputs
        index_array = np.array(self.convertToEncoderSeq(batch_seqs)).T # converted inputs (may be reversed)
        index_array_pair_sent1 = np.array(self.convertToEncoderSeq(batch_pair_sent1)).T # converted inputs (may be reversed)
        index_array_pair_sent2 = np.array(self.convertToEncoderSeq(batch_pair_sent2)).T # converted inputs (may be reversed)
        index_array_pair_dec = np.array(index_pair_dec[:]).T
        index_array_pair_inc = np.array(index_pair_inc[:]).T
        feed_dict = {self.enc_inp[t]: index_array[t] for t in range(self.max_seq_length)}
        feed_dict.update({self.labels[t]: index_array2[t] for t in range(self.max_seq_length)})
        feed_dict.update({self.outcomes: batch_outcomes})
        feed_dict.update({self.outcomes_sent1: batch_outcomes_sent1})
        feed_dict.update({self.outcomes_sent2: batch_outcomes_sent2})
        feed_dict.update({self.seq2seq_importance: seq2seq_importanceval})
        feed_dict.update({self.kl_importance: kl_importanceval})
        feed_dict.update({self.epsilon_vae: epsilons})
        feed_dict.update({self.pair_inp1[t]: index_array_pair_sent1[t] for t in range(self.max_seq_length)})
        feed_dict.update({self.pair_inp2[t]: index_array_pair_sent2[t] for t in range(self.max_seq_length)})
        feed_dict.update({self.labels1[t]: index_array2_pair_sent1[t] for t in range(self.max_seq_length)})
        feed_dict.update({self.labels2[t]: index_array2_pair_sent2[t] for t in range(self.max_seq_length)})
        feed_dict.update({self.pair_inc: index_array_pair_inc})
        feed_dict.update({self.pair_dec: index_array_pair_dec})
        feed_dict.update({self.epsilon_vae_content: epsilons_content})
        feed_dict.update({self.pair_importance_diff: pair_importance_diff})
        feed_dict.update({self.pair_importance_sim: pair_importance_sim})
        feed_dict.update({self.discrim_importance: discrim_importance})
        feed_dict.update({self.invar_importance: invar_importanceval})
        if invar_importanceval > 0: # Train E, F w.r.t. invariance loss:
            inv_target_vals, inv_inputs = self.get_invar_inputs(invar_batchsize)
            feed_dict.update({self.invar_inp[t]: inv_inputs[t] for t in range(self.max_seq_length)})
            feed_dict.update({self.invar_target: inv_target_vals})
            _, train_loss, lik_loss, pri_loss, pred_loss, sents_diff_loss, sents_sim_loss, discrim_loss, inv_loss, summary = self.sess.run([self.train_joint, self.loss_joint, self.loss_lik, self.loss_prior, self.loss_pred, self.loss_diff, self.loss_sim, self.loss_discrim, self.loss_inv, self.summary_op], feed_dict)
        else:
            _, train_loss, lik_loss, pri_loss, pred_loss, sents_diff_loss, sents_sim_loss, discrim_loss, summary = self.sess.run([self.train_joint, self.loss_joint, self.loss_lik, self.loss_prior, self.loss_pred, self.loss_diff, self.loss_sim, self.loss_discrim,  self.summary_op], feed_dict)
            inv_loss = 0
        return(train_loss, lik_loss, pri_loss, pred_loss, sents_diff_loss, sents_sim_loss, discrim_loss, inv_loss, summary)

    """ Use model for prediction (done in batches).
    Args:
        test_outcomes: the actual labels (optional).
    Returns: A tuple of form (predicted_outcomes, reconstructed_seqs, avg_outcome_error, avg_reconstruction_error)
    where avg_outcome_error is only computed if test_outcomes is provided.
    """
    def predict(self, test_seqs, test_outcomes = None, batch_size = 64):
        n = len(test_seqs)
        if (test_outcomes is not None) and len(test_outcomes) != n:
            raise ValueError("test_outcomes and test_seqs do not agree")
        predicted_outcomes = []
        reconstructed_seqs = []
        outcome_errors = []
        reconstruction_errors = []
        sigmas_pred = []
        ind = 0
        while ind < n: # Train batch:
            batch_inds = range(ind, min(n,ind+batch_size))
            if test_outcomes is None:
                batch_outcomes = None
            else:
                batch_outcomes = test_outcomes[ind:min(n,ind+batch_size)]
            b_predicted_outcomes, b_reconstructed_seqs, b_outcome_errors, b_reconstruction_errors, b_sigmas_pred = self.predict_batch(test_seqs[ind:min(n,ind+batch_size)], batch_outcomes)
            predicted_outcomes += b_predicted_outcomes
            reconstructed_seqs += b_reconstructed_seqs
            reconstruction_errors += b_reconstruction_errors
            if test_outcomes is not None:
                outcome_errors += b_outcome_errors
            sigmas_pred += [b_sigmas_pred]
            ind = ind + batch_size
        return(predicted_outcomes, reconstructed_seqs,
                np.mean(np.array(outcome_errors)), 
                np.mean(np.array(reconstruction_errors)), 
                np.mean(np.array(sigmas_pred)))
    
    """ Performs prediction for a single batch. Evaluates errors for each example in batch.
    Args:
    Returns: A tuple (predicted_outcomes, reconstructed_seqs, outcome_errors, reconstruction_errors)
             where outcome_errors, reconstruction_errors = list of error for each example.
    """
    def predict_batch(self, batch_seqs, batch_outcomes = None):
        index_array_og = np.array(self.convertToIndexSeq(batch_seqs)).T # reformatted seq in terms of indices in vocab, NOT reversed.  Dimshuffle to seq_len * batch_size
        index_array = np.array(self.convertToEncoderSeq(batch_seqs)).T # converted inputs, could be reversed
        feed_dict = {self.enc_inp[t]: index_array[t] for t in range(self.max_seq_length)}
        decoding_scores, outcome_predictions, pred_sigmas = self.sess.run([self.decodingprobs0, self.outcome0, self.sigma0], feed_dict)
        decodings = np.array([logits_t.argmax(axis=1) for logits_t in decoding_scores])
        if batch_outcomes is not None:
            batch_outcomes = np.array(batch_outcomes)
            batch_outcomes = batch_outcomes.reshape(len(batch_outcomes),1)
            # outcome_errors = np.abs(outcome_predictions - batch_outcomes)
            outcome_errors = np.square(outcome_predictions - batch_outcomes)
            outcome_errors = [outcome_errors[i][0] for i in range(len(batch_seqs))]
        else:
            outcome_errors = None
        reconstruction_errors = list(np.sum(index_array_og != decodings,axis=0)) # count of number of errors in each sentence
        predicted_outcomes = [outcome_predictions[i][0] for i in range(len(batch_seqs))]
        reconstructed_seqs = [[self.vocab[vocab_index] for vocab_index in list(decodings[:,i])] for i in range(len(batch_seqs))]
        return((predicted_outcomes, reconstructed_seqs, outcome_errors, reconstruction_errors, np.mean(pred_sigmas)))

    """ Takes fixed number of gradient steps to produce revision
        of the given input sequence.
    Args:
    input_seq: list of length 1 containing the sequence to be revised (must be list!!!)
    outcomeopt_learn_rate: the learning rate used in the outcome-optimization
    max_outcomeopt_iter: the number of gradient steps to take in outcome-optimization
    print_iter: print objective val every so often (set = 0 or False to turn off printing)
    Returns:
    x_star: the optimal sequence
    z_star: the optimal latent latent_representation
    improvement: the amount of predicted improvement achieved
    objective: the predicted outcome for z_star
    z_init: the initial latent representation 
    outcome_init: the predicted outcome for z_init
    """
    def fixedGradientRevise(self, input_seq, outcomeopt_learn_rate = 0.05, 
               max_outcomeopt_iter = 1000, print_iter = None):
        if self.which_outcomeprediction == 'linear':
            warnings.warn("Linear outcome prediction used with gradientRevise")
        if print_iter is None:
            print_iter = math.ceil(max_outcomeopt_iter/10.0)
        index_seq = self.convertToIndexSeq(input_seq)
        input_trans = np.array(index_seq).T
        feed_dict = {self.enc_inp[t]: input_trans[t] for t in range(len(index_seq[0]))}
        outcome_init, z_val, sigmas_init  = self.sess.run([self.outcome0,self.z0,self.sigma0], feed_dict)
        sigmas = sigmas_init[0]
        avg_sigma_init = np.mean(sigmas)
        z_init = np.copy(z_val)
        reconstruct_init = self.decode(z_init, input_trains, feed_dict)

        for i in range(max_outcomeopt_iter):
            g, obj = self.sess.run([self.z_grad, self.inferred_outcome], feed_dict={self.z: z_val})
            # TODO: normalize the gradient, so the same step size should work 
            z_val += g*outcomeopt_learn_rate
            if (print_iter > 0) and (i % print_iter == 0):
                print('iter=', i, '  obj=', obj - outcome_init)
        z_star = np.copy(z_val)
        x_star = self.decode(z_star, feed_dict)
        edit_dist = levenshtein(input_seq[0], x_star)
        obj = np.ravel(obj)[0]
        outcome_init = np.ravel(outcome_init)[0]
        return(x_star, z_star, obj - outcome_init, obj, reconstruct_init, z_init, outcome_init, avg_sigma_init, edit_dist)
    
    """ Uses barrier method + gradient ascent to produce revision
        of the given input sequence within constraint-set specified by posterior.
    Args:
    input_seq: list of length 1 containing the sequence to be revised (must be list!!!)
    outcomeopt_learn_rate: the learning rate used in the outcome-optimization
    max_outcomeopt_iter: the number of gradient steps to take in outcome-optimization
    print_iter: print objective val every so often (set = 0 or False to turn off printing)
    Returns:
    x_star: the optimal sequence
    z_star: the optimal latent latent_representation
    improvement: the amount of predicted improvement achieved
    objective: the predicted outcome for z_star
    z_init: the initial latent representation 
    outcome_init: the predicted outcome for z_init
    """
    def barrierGradientRevise(self, input_seq, log_alpha, outcomeopt_learn_rate = 0.05, 
               max_outcomeopt_iter = 1000, print_iter = None, use_adaptive = False):
        if self.which_outcomeprediction == 'linear':
            warnings.warn("Linear outcome prediction used with barrierGradientRevise")
        if print_iter is None:
            print_iter = math.ceil(max_outcomeopt_iter/10.0)
        index_seq = self.convertToIndexSeq(input_seq)
        input_trans = np.array(index_seq).T
        feed_dict1 = {self.enc_inp[t]: input_trans[t] for t in range(len(index_seq[0]))}
        outcome_init, z_val, sigmas_init  = self.sess.run([self.outcome0,self.z0,self.sigma0], feed_dict1)
        sigmas = sigmas_init[0]
        avg_sigma_init = np.mean(sigmas)
        z_init = np.copy(z_val)
        reconstruct_init = self.decode(z_init, feed_dict1)
        # Get constraint-ellipse:
        min_sigma_threshold = 1e-2
        for i in range(len(sigmas)):
            if sigmas[i] < min_sigma_threshold:
                sigmas[i] = min_sigma_threshold
        log_alpha = log_alpha - (self.cell.state_size/2)*np.log(2*np.pi)
        sigmas_sq = np.square(sigmas)
        Covar = np.diag(sigmas_sq)
        max_log_alpha = -0.5 * np.sum(np.log(2*np.pi*sigmas_sq))
        if log_alpha > max_log_alpha:
            warnings.warn('log_alpha = %f is too large (max = %f will return no revision.' % (log_alpha, max_log_alpha))
            return(input_seq[0], z_init, 0.0, np.ravel(outcome_init)[0], reconstruct_init, z_init, np.ravel(outcome_init)[0],avg_sigma_init, 0.0)
        K = -2* (np.log(np.power(2*np.pi,self.cell.state_size/2)) + 0.5*np.sum(np.log(sigmas_sq)) + log_alpha)
        A= np.linalg.pinv(Covar) / K  # A is matrix s.t. z^T A z < 1 corresponds to alpha-contour of N(0, diag(sigmas))
        convergence_thresh = 1e-8
        last_obj = -1e6
        feed_dict = {self.zfirst: z_init}
        feed_dict.update({self.Amat: A})
        for i in range(max_outcomeopt_iter):
            feed_dict.update({self.z: z_val})
            g, obj, inferred_outcome_val = self.sess.run([self.z_grad, self.revision_barrier_obj, self.inferred_outcome], feed_dict)
            stepsize = outcomeopt_learn_rate*1000/(1000+np.sqrt(i))
            violation = True
            while violation and (stepsize >= convergence_thresh/100.0):
                z_proposal = z_val + g*stepsize
                shift = z_proposal - z_init
                if np.dot(np.dot(shift, A),shift.transpose()) < 1:  # we are inside constraint-set
                    violation = False
                else:
                    stepsize /= 2.0  # keep dividing by 2 until we remain within constraint
            if stepsize < convergence_thresh/100.0:
                break # break out of for loop.
            z_val = z_proposal
            '''
            if (print_iter > 0) and (i % print_iter == 0):
                print('iter=', i, '  obj=', obj - outcome_init)
            '''
            if np.abs(obj - last_obj) < convergence_thresh:
                break
            last_obj = obj
        z_star = np.copy(z_val)
        if use_adaptive:
            print_beta = True
            x_star = self.adaptiveDecode(z_star, input_seq, print_beta = print_beta)
        else:
            x_star = self.decode(z_star, feed_dict1)
        edit_dist = levenshtein(input_seq[0], x_star)
        inferred_outcome_val = np.ravel(inferred_outcome_val)[0]
        outcome_init = np.ravel(outcome_init)[0]
        if print_iter > 0:
            shift = z_star - z_init
        #    print("Elliptical constraint value:", np.dot(np.dot(shift, A),shift.transpose())) # should be  < 1
        return(x_star, z_star, inferred_outcome_val - outcome_init, inferred_outcome_val, reconstruct_init, z_init, outcome_init, avg_sigma_init, edit_dist)
    
    
    """ Performs decoding from a given latent state Z.
        feed_dict should contain the orginal sequence.
        Returns sequence in original vocabulary with padding removed.
    """
    def decode(self, z_encoding, feed_dict):
        feed_dict.update({self.z: z_encoding})
        decoded_probs = self.sess.run(self.x_hat, feed_dict)
        decoded_numseq = np.array([logits_t.argmax(axis=1) for logits_t in decoded_probs])
        decoded_vocabseq = [self.vocab[vocab_index] for vocab_index in list(decoded_numseq[:,0])]
        if '<PAD>' in decoded_vocabseq:  # remove everything after PAD char
            first_pad = decoded_vocabseq.index('<PAD>')
            decoded_vocabseq = decoded_vocabseq[:first_pad]
        return(decoded_vocabseq)
    
    """ Decoding from a given latent state Z which is biased toward x0 (must be a list of seq)
        feed_dict should contain the orginal sequence.
        Returns sequence in original vocabulary with padding removed.
        x0: input sequence (a list of lists of length=1).
    """
    def adaptiveDecode(self, z_encoding, x0, print_beta = False):
        betas = self.computeBetas(x0, print_beta)
        og_index_seq = self.convertToIndexSeq(x0)
        og_input_trans = np.array(og_index_seq).T
        for position in range(self.max_seq_length): # where we are decoding currently:
            feed_dict = {self.enc_inp[t]: og_input_trans[t] if t >= position else new_input_trans[t]
                                          for t in range(self.max_seq_length)}
            feed_dict.update({self.z: z_encoding})
            decoded_probs = self.sess.run(self.x_hat, feed_dict)
            # Bias decoded_probs using betas:
            decoded_probs = self.applyBetas(betas, decoded_probs, x0)
            decoded_numseq = np.array([logits_t.argmax(axis=1) for logits_t in decoded_probs])  
            decoded_vocabseq = [self.vocab[vocab_index] for vocab_index in list(decoded_numseq[:,0])]
            new_index_seq = self.convertToIndexSeq([decoded_vocabseq])
            new_input_trans = np.array(new_index_seq).T
        if '<PAD>' in decoded_vocabseq:  # remove everything after PAD char
            first_pad = decoded_vocabseq.index('<PAD>')
            decoded_vocabseq = decoded_vocabseq[:first_pad]
        return(decoded_vocabseq)
    
    def computeBetas(self, x0, print_beta = False):
        """ Identifies betas needed for adaptive decoding."""
        index_seqs = self.convertToIndexSeq(x0)  # reformatted seq in terms of indices in vocab
        index_array2 = np.array(index_seqs[:]).T  # NON-converted inputs
        index_array = np.array(self.convertToEncoderSeq(x0)).T  # converted inputs (may be reversed)
        feed_dict = {self.enc_inp[t]: index_array[t] for t in range(self.max_seq_length)}
        feed_dict.update({self.labels[t]: index_array2[t] for t in range(self.max_seq_length)})
        decoding_scores = self.sess.run(self.decodingprobs0, feed_dict)
        betas = np.array([0.0]*self.max_seq_length)
        max_decodings = [logits_t.max(axis=1) for logits_t in decoding_scores]
        pad_index = self.vocab.index('<PAD>')
        for t in range(self.max_seq_length):
            if t < len(x0[0]):
                x0_index_t = self.vocab.index(x0[0][t]) # index of the t-th symbol in x0
            else:
                x0_index_t = pad_index
            if decoding_scores[t][0][x0_index_t] < max_decodings[t][0]:
                betas[t] = max_decodings[t] - decoding_scores[t][0][x0_index_t] + 1e-3
        if print_beta:
            print('betas:',betas)
        return betas
    
    def applyBetas(self, betas, decoded_probs, x0):
        new_probs = decoded_probs[:]
        pad_index = self.vocab.index('<PAD>')
        for t in range(self.max_seq_length):
            if t < len(x0[0]):
                x0_index_t = self.vocab.index(x0[0][t]) # index of the t-th symbol in x0
            else:
                x0_index_t = pad_index
            new_probs[t][0][x0_index_t] = decoded_probs[t][0][x0_index_t] + betas[t]
        return new_probs
    
    """ Reformats sequence in terms of vocab-indices for embedding RNNs """        
    def convertToIndexSeq(self, seqs):
        return([np.array(self.pad_sequence(self.index_sequence(seq))) for seq in seqs])
    
    """ Reformats sequence in terms of REVERSED vocab-indices for encoder RNN """        
    def convertToEncoderSeq(self, seqs, reverse_seq = False):
        if reverse_seq:
            return([np.array(self.pad_sequence(self.index_sequence(seq))[::-1]) for seq in seqs])
        else:
            return([np.array(self.pad_sequence(self.index_sequence(seq))) for seq in seqs])
    
    """ Converts strings to integer indices in vocabulary """
    def index_sequence(self, seq):
        if len(seq) > self.max_seq_length:
            raise ValueError("batch contains sequence of length > max_seq_length")
        elif self.use_unknown:
            index_seq = [self.vocab.index(s) if s in self.vocab else self.UNK_ID for s in seq]
        else:
            index_seq = []
            for s in seq:
                if s in self.vocab:
                     index_seq.append(self.vocab.index(s))
                else:
                    raise ValueError('Unknown symbol: ' + str(s))
        return(index_seq)
    
    """ Pads Index sequence to max_seq_length (padding added to end of sequence) """
    def pad_sequence(self, index_seq):
        while len(index_seq) < self.max_seq_length:
            index_seq.append(self.PAD_ID)
        return(index_seq)
    
    """ Saves model to file """
    def save(self, save_name):
        self.saver.save(self.sess, save_name)
        print("Model saved as: " + save_name)
    
    """ Restores model from file """
    def restore(self, save_name):
        self.saver.restore(self.sess, save_name)
        print("Model restored from: " + save_name)
    
    """ Calculate using variance in outcomes from training data """
    def setOutcomeVar(self, variance_val):
        self.outcome_var = variance_val
        print("outcome_var set to:", self.outcome_var)
    
    """ Set learning rate for parameter-training to given value """
    def resetLearnRate(self, new_learn_rate):
        self.learning_rate = new_learn_rate
        self.optimizer = tf.train.AdamOptimizer(learning_rate = self.learning_rate)
        print("learning rate changed to:", new_learn_rate)

    def get_invar_inputs(self, invar_batchsize):
        z_externals = np.random.normal(size=[invar_batchsize,self.cell.state_size])
        decoded_seqs = []
        index_array = np.array(self.convertToEncoderSeq([[self.vocab[0],self.vocab[0]]])).T # converted random input
        feed_dict = {self.enc_inp[t]: index_array[t] for t in range(self.max_seq_length)}
        for b in range(invar_batchsize):
            z_external = z_externals[b,:].reshape([1,self.cell.state_size])
            decoded_seq = self.decode(z_external, feed_dict = feed_dict)
            decoded_seqs.append(decoded_seq)
        inv_inputs = np.array(self.convertToEncoderSeq(decoded_seqs)).T
        external_outcomes = []
        for b in range(invar_batchsize):
            z_external = z_externals[b,:].reshape([1,self.cell.state_size])
            feed_dict.update({self.z: z_external})
            outcome_external = self.sess.run(self.inferred_outcome, feed_dict)
            external_outcomes.append(outcome_external)
        inv_target_vals = np.array(external_outcomes).reshape([invar_batchsize, 1])
        return(inv_target_vals, inv_inputs)
    def test_content_var(self, input_seq, input_sent1, input_sent2):
        index_seq = self.convertToIndexSeq(input_seq)
        input_trans = np.array(index_seq).T
        index_seq_sent1 = self.convertToIndexSeq(input_sent1)
        index_seq_sent2 = self.convertToIndexSeq(input_sent2)
        input_trans_sent1 = np.array(index_seq_sent1).T
        input_trans_sent2 = np.array(index_seq_sent2).T
        feed_dict1 = {self.enc_inp[t]: input_trans[t] for t in range(len(index_seq[0]))}
        feed_dict1.update({self.pair_inp1[t]: input_trans_sent1[t] for t in range(self.max_seq_length)})
        feed_dict1.update({self.pair_inp2[t]: input_trans_sent2[t] for t in range(self.max_seq_length)})
        content, content_sent1, content_sent2 ,loss_sim= self.sess.run([self.z0_content, self.z0_content_sent1, self.z0_content_sent2, self.loss_sim], feed_dict1)
        sim = tf.square(content_sent1 - content_sent2)
        mean = tf.reduce_mean(sim)
        print (content)
        print (content_sent1)
        print (content_sent2)
        print (content_sent1 - content_sent2)
        print (loss_sim)
        print (sim)
        print (mean)
        print ("\n")

